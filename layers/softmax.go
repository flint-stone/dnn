package layers

import (
	"math"

	"gitlab.com/akita/dnn/tensor"
)

// A SoftmaxLayer runs the softmax algorithm.
type SoftmaxLayer struct {
	Size int

	forwardInput []float64
}

// NewSoftmaxLayer creates a new softmax layer.
func NewSoftmaxLayer(size int) *SoftmaxLayer {
	return &SoftmaxLayer{Size: size}
}

func (s *SoftmaxLayer) Randomize() {
	// This function is intentionally left blank.
}

func (s *SoftmaxLayer) Forward(inputTensor tensor.Tensor) tensor.Tensor {
	input := inputTensor.(*tensor.SimpleTensor)
	size := input.Size()

	output := &tensor.SimpleTensor{}
	output.Init(make([]float64, len(input.Vector())), size)

	s.saveInput(input)

	for i := 0; i < size[0]; i++ {
		start := i * size[1]
		end := (i + 1) * size[1]
		inVector := input.Vector()[start:end]
		outVector := softmax(inVector)
		copy(output.Vector()[start:end], outVector)
	}

	return output
}

func (s *SoftmaxLayer) saveInput(input *tensor.SimpleTensor) {
	s.forwardInput = input.Vector()
}

func (s SoftmaxLayer) Backward(inputTensor tensor.Tensor) tensor.Tensor {
	input := inputTensor.(*tensor.SimpleTensor)
	size := input.Size()
	output := &tensor.SimpleTensor{}
	output.Init(make([]float64, len(input.Vector())), size)

	for i := 0; i < output.Size()[0]; i++ {
		start := i * size[1]
		end := (i + 1) * size[1]
		array := s.forwardInput[start:end]
		softmaxArray := softmax(array)
		for j := 0; j < output.Size()[1]; j++ {
			sum := float64(0)
			for k := 0; k < output.Size()[1]; k++ {
				diff := float64(0)
				if k == j {
					diff = softmaxArray[k] * (1 - softmaxArray[k])
				} else {
					diff = -softmaxArray[k] * softmaxArray[j]
				}
				sum += input.Vector()[start+k] * diff
			}
			output.Vector()[start+j] = sum
		}
	}

	return output
}

func (s SoftmaxLayer) Parameters() tensor.Vector {
	return nil
}

func (s SoftmaxLayer) Gradients() tensor.Vector {
	return nil
}

func softmax(array []float64) []float64 {
	denominator := softmaxDenominator(array)
	out := make([]float64, len(array))

	for i := 0; i < len(array); i++ {
		out[i] = math.Exp(array[i]) / denominator
	}

	return out
}

func softmaxDenominator(array []float64) float64 {
	denominator := float64(0)
	for i := 0; i < len(array); i++ {
		denominator += math.Exp(array[i])
	}
	return denominator
}
