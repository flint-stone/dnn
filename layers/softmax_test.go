package layers

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/akita/dnn/tensor"
)

var _ = Describe("Softmax Layer", func() {
	var (
		layer *SoftmaxLayer
		input *tensor.SimpleTensor
	)

	BeforeEach(func() {
		input = &tensor.SimpleTensor{}
		layer = NewSoftmaxLayer(2)
	})

	It("should forward", func() {
		input.Init([]float64{
			1, -1, 2,
			2, 3, 4,
		}, []int{2, 3})

		output := layer.Forward(input)

		expectedOutput := []float64{
			0.259, 0.0351, 0.705,
			0.0900, 0.245, 0.665,
		}
		Expect(output.Size()).To(Equal([]int{2, 3}))
		for i := range input.Vector() {
			Expect(output.Vector()[i]).
				To(BeNumerically("~", expectedOutput[i], 0.01))
		}

		Expect(layer.forwardInput).To(Equal(input.Vector()))
	})

	It("should backward", func() {
		input.Init([]float64{
			0.1, 0.2, 0.3,
			0.4, 0.5, 0.6,
		}, []int{2, 3})
		layer.forwardInput = []float64{
			1, -1, 2,
			2, 3, 4,
		}

		output := layer.Backward(input)

		Expect(output.Size()).To(Equal([]int{2, 3}))
		expectedOutput := []float64{
			-0.0375, -0.00157, 0.0391,
			-0.0142, -0.0141, 0.0283,
		}
		for i := range input.Vector() {
			Expect(output.Vector()[i]).
				To(BeNumerically("~", expectedOutput[i], 0.01))
		}
	})
})
