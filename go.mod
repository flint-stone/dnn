module gitlab.com/akita/dnn

go 1.13

require (
	github.com/golang/mock v1.3.1
	github.com/onsi/ginkgo v1.11.0
	github.com/onsi/gomega v1.8.1
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gonum.org/v1/gonum v0.6.2
)
